package com.apm.apm.test;

import com.apm.apm.test.model.GetTestResponse;
import com.apm.apm.test.model.SaveTestRequest;
import com.apm.apm.test.model.UpdateTestRequest;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TestMapper {

    Test toEntity(SaveTestRequest request);

    Test toEntity(UpdateTestRequest request);

    GetTestResponse toGetResponse(Test test);
}
