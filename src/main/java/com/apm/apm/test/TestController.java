package com.apm.apm.test;

import com.apm.apm.test.model.GetAllTestResponse;
import com.apm.apm.test.model.GetTestResponse;
import com.apm.apm.test.model.SaveTestRequest;
import com.apm.apm.test.model.UpdateTestRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {

    private final TestService testService;

    public TestController(TestService testService) {
        this.testService = testService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public GetAllTestResponse getAll() {
        log.info("Getting all tests");
        return this.testService.getAll();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public GetTestResponse getById(@PathVariable("id") String id) {
        log.info("Getting test with id: {}", id);
        return this.testService.get(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public GetTestResponse save(@RequestBody SaveTestRequest request) {
        log.info("Saving test: {}", request);
        return this.testService.save(request);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public GetTestResponse update(@RequestBody UpdateTestRequest request) {
        log.info("Updating test: {}", request);
        return this.testService.update(request);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") String id) {
        log.info("Deleting test with id: {}", id);
        this.testService.delete(id);
    }
}
